<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\StaticCacheModule\Core;

use JsonException;

/**
 * class for output processing.
 */
class Output extends Output_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws JsonException
     */
    public function process($sValue, $sClassName)
    {
        $sValue = parent::process($sValue, $sClassName);

        if (!isAdmin()) {
            $oCache = oxNew(StaticCache::class);
            $oCache->buildCache($sValue);
        }

        return $sValue;
    }
}
