<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\StaticCacheModule\Core;

use JsonException;
use OxidEsales\Eshop\Application\Model\User;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Core\ToolsFile;
use TheRealWorld\ToolsPlugin\Core\ToolsString;

/**
 * class to generate or to output static cache.
 */
class StaticCache
{
    /** Array with all cacheable Controllers */
    protected array $_aDefaultCacheableControllers = [
        'startcontroller',
        'articlelistcontroller',
        'articledetailscontroller',
        'linkscontroller',
        'contentcontroller',
    ];

    /**
     * set the active Class Name for Request.
     *
     * @param null|string $sClassLong (Classname with namespaces)
     */
    public function setActiveClass(?string $sClassLong = null): void
    {
        StaticCacheConfig::setActiveClass($sClassLong);
    }

    /**
     * set the active Function Name for Request.
     */
    public function setActiveFunction(?string $sFunction = null): void
    {
        StaticCacheConfig::setActiveFunction($sFunction);
    }

    /**
     * Cache Processor.
     *
     * @throws JsonException
     */
    public function processCache(): void
    {
        if (!$this->isCacheableRequest()) {
            return;
        }
        $sCachePath = StaticCacheConfig::getCacheFileName();
        if (!is_file($sCachePath)) {
            return;
        }
        $sCacheData = file_get_contents($sCachePath);
        $oCacheData = json_decode($sCacheData, false, 512, JSON_THROW_ON_ERROR);

        $iCacheTime = $oCacheData->timestamp;
        $content = $oCacheData->content;

        if (time() < $iCacheTime + StaticCacheConfig::getCacheLifetime()) {
            exit($content);
        }
        unlink($sCachePath);
    }

    /**
     * build the Cache File.
     *
     * @param string $sContent Cacheable String
     *
     * @throws JsonException
     */
    public function buildCache(string $sContent): void
    {
        if (!$this->isCacheableRequest()) {
            return;
        }
        $sContent = $this->minifyHtml($sContent);

        $aCacheData = [
            'controller' => StaticCacheConfig::getActiveClass(),
            'content'    => $this->addCacheVersionTags($sContent),
            'timestamp'  => time(),
        ];

        ToolsFile::createPath(StaticCacheConfig::getCachePath(), false);

        $sCacheData = json_encode($aCacheData, JSON_THROW_ON_ERROR);
        $sCacheFileName = StaticCacheConfig::getCacheFileName();

        file_put_contents($sCacheFileName, $sCacheData);
    }

    /**
     * add a Cache Version Tag to the output.
     */
    protected function addCacheVersionTags(string $sOutput): string
    {
        $oConfig = Registry::getConfig();
        // display it
        $sVersion = $oConfig->getVersion();
        $sEdition = $oConfig->getFullEdition();
        $sCurYear = date('Y');
        $sShopMode = '';

        // show only major version number
        $aVersion = explode('.', $sVersion);
        $sMajorVersion = reset($aVersion);

        // Copyright
        if ($oConfig->getConfigParam('bTRWCacheShowCopyright')) {
            $sOutput = str_ireplace(
                '</head>',
                "</head>\n<!-- OXID eShop {$sEdition}," .
                "Version {$sMajorVersion}{$sShopMode}," .
                'Shopping Cart System (c) OXID eSales AG 2003 ' .
                "- {$sCurYear} - https://www.oxid-esales.com -->\n",
                ltrim($sOutput)
            );
        }

        return $sOutput;
    }

    /** Check if it is a cacheable Request */
    protected function isCacheableRequest(): bool
    {
        if (!$this->isCacheForControllerActive()) {
            return false;
        }
        if (StaticCacheConfig::getActiveFunction()) {
            return false;
        }
        $oUser = oxNew(User::class);
        if ($oUser->loadActiveUser() !== false) {
            return false;
        }
        $oBasket = Registry::getSession()->getBasket();
        if ($oBasket && $oBasket->getItemsCount()) {
            return false;
        }
        if (!$this->isCacheForSessionAllowed()) {
            return false;
        }
        if (!$this->isCacheForCookiesAllowed()) {
            return false;
        }

        return true;
    }

    /** is cache for controller active? */
    protected function isCacheForControllerActive(): bool
    {
        $bResult = false;
        $oConfig = Registry::getConfig();
        $aAdditionalCaches = $oConfig->getConfigParam('aTRWAdditionalCaches');
        if (
            ToolsString::inArrayI(
                StaticCacheConfig::getActiveClass(),
                $this->_aDefaultCacheableControllers
            )
        ) {
            $bResult = $oConfig->getConfigParam('bTRWCache' . StaticCacheConfig::getActiveClass());
        } elseif (
            is_array($aAdditionalCaches)
            && ToolsString::inArrayI(
                StaticCacheConfig::getActiveClass(),
                $aAdditionalCaches
            )
        ) {
            $bResult = true;
        }

        return $bResult;
    }

    /** is cache for Session allowed */
    protected function isCacheForSessionAllowed(): bool
    {
        $bResult = true;
        $aNotAllowedSessionVars = Registry::getConfig()->getConfigParam('aTRWNotAllowedSessionVars');
        foreach ($aNotAllowedSessionVars as $sNotAllowedSessionVar) {
            if (Registry::getSession()->hasVariable($sNotAllowedSessionVar)) {
                $bResult = false;

                break;
            }
        }

        return $bResult;
    }

    /** is cache for Cookies allowed */
    protected function isCacheForCookiesAllowed(): bool
    {
        $bResult = true;
        $aNotAllowedCookies = Registry::getConfig()->getConfigParam('aTRWNotAllowedCookieVars');
        foreach ($aNotAllowedCookies as $sNotAllowedCookie) {
            if (Registry::getUtilsServer()->getOxCookie($sNotAllowedCookie)) {
                $bResult = false;

                break;
            }
        }

        return $bResult;
    }

    /** minify the outputed Html.
     *
     * @param string $sValue htmlcode
     */
    protected function minifyHtml(string $sValue): string
    {
        $aSearch = ['/ {2,}/', '/<!--.*?-->|\t|(?:\r?\n[ \t]*)+/s'];
        $aReplace = [' ', ' '];

        return Str::getStr()->preg_replace($aSearch, $aReplace, $sValue);
    }
}
