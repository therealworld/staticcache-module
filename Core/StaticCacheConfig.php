<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\StaticCacheModule\Core;

use JsonException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Core\ToolsString;

/**
 * class to generate or to output static cache.
 */
class StaticCacheConfig
{
    /** actual Request Controller Class */
    protected static ?string $_sClass = null;

    /** actual Request Function */
    protected static ?string $_sFunction = null;

    /** actual Request Function */
    protected static ?string $_sStrParams = null;

    /** get the File Name of the Cache.
     * @throws JsonException
     */
    public static function getCacheFileName(): string
    {
        $sRequestUrl = Registry::getUtilsServer()->getServerVar('REQUEST_URI');
        $oConfig = Registry::getConfig();
        $sSSL = ($oConfig->isSsl() ? 'ssl' : '');

        $sParams = self::getStrParams();
        $sFileName = md5($sSSL . $sParams . $sRequestUrl);

        return self::getCachePath() . $sFileName . '.json';
    }


    /** get the Path of the Cache */
    public static function getCachePath(): string
    {
        return Registry::getConfig()->getConfigParam('sCompileDir') . 'trwcache' . DIRECTORY_SEPARATOR;
    }

    /** get the Lifetime of the Cache */
    public static function getCacheLifetime(): int
    {
        $iLifetime = Registry::getConfig()->getConfigParam('iTRWCacheLifetime');
        return is_numeric($iLifetime) ? (int) $iLifetime : 0;
    }

    /**
     * set the active Class Name for Request.
     *
     * @param null|string $sClassLong (Classname with namespaces)
     */
    public static function setActiveClass(?string $sClassLong = null): void
    {
        $sClassLong ??= '';
        self::$_sClass = Str::getStr()->strtolower(ToolsString::splitOnLastOccurrence($sClassLong, chr(92)));
    }

    /** get the active Class name */
    public static function getActiveClass(): string
    {
        if (!isset(self::$_sClass)) {
            self::setActiveClass(Registry::getConfig()->getActiveView()->getThisAction());
        }
        return self::$_sClass;
    }

    /**
     * set the active Function Name for Request.
     */
    public static function setActiveFunction(?string $sFunction = null): void
    {
        self::$_sFunction = $sFunction;
    }

    /** get the active Class name */
    public static function getActiveFunction(): ?string
    {
        return self::$_sFunction;
    }

    /**
     * has the controller important cacheable Params,
     * so we can create a possible string for prepare a special part
     * of the cacheName.
     *
     * @throws JsonException
     */
    protected static function getStrParams(): string
    {
        if (isset(self::$_sStrParams)) {
            return self::$_sStrParams;
        }

        $aParams = [];

        $iLang = Registry::getLang()->getBaseLanguage();
        $oConfig = Registry::getConfig();
        $oSession = Registry::getSession();
        $oRequest = Registry::getRequest();

        $aParams['lang'] = $iLang;

        if (self::getActiveClass() === 'articlelistcontroller') {
            $sActCat = $oRequest->getRequestParameter('cnid');
            $aSessionFilter = $oSession->getVariable('session_attrfilter');
            $aParams['session_attrfilter'] = $aSessionFilter[$sActCat][$iLang];
        }

        $aAllowedSessionVars = $oConfig->getConfigParam('aTRWAllowedSessionVars');
        foreach ($aAllowedSessionVars as $sAllowedSessionVar) {
            $aParams['allowed_session_var'][$sAllowedSessionVar] = $oSession->getVariable($sAllowedSessionVar);
        }

        $aAllowedCookieVars = $oConfig->getConfigParam('aTRWAllowedCookieVars');
        foreach ($aAllowedCookieVars as $sAllowedCookieVar) {
            $allowedCookie = Registry::getUtilsServer()->getOxCookie($sAllowedCookieVar);
            if (is_string($allowedCookie)) {
                // remove the quotes set by getOxCookie
                $allowedCookie = str_replace(['amp;', '&quot;'], ['', '"'], $allowedCookie);
            }
            $aParams['allowed_cookie_var'][$sAllowedCookieVar] = $allowedCookie;
        }

        $aAllowedRequestVars = $oConfig->getConfigParam('aTRWAllowedRequestVars');
        foreach ($aAllowedRequestVars as $sAllowedRequestVar) {
            $aParams['allowed_request_var'][$sAllowedRequestVar] = $oRequest->getRequestParameter($sAllowedRequestVar);
        }

        self::$_sStrParams = (string) json_encode($aParams, JSON_THROW_ON_ERROR);

        return self::$_sStrParams;
    }
}