<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\StaticCacheModule\Core;

use JsonException;

/**
 * Main shop actions controller. Processes user actions, logs
 * them (if needed), controlls output, redirects according to
 * processed methods logic. This class is initalized from index.php.
 */
class ShopControl extends ShopControl_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws JsonException
     */
    protected function _process($sClass, $sFunction, $aParams = null, $aViewsChain = null)
    {
        if (!isAdmin()) {
            $oCache = oxNew(StaticCache::class);
            $oCache->setActiveClass($sClass);
            $oCache->setActiveFunction($sFunction);
            $oCache->processCache();
        }
        parent::_process($sClass, $sFunction, $aParams, $aViewsChain);
    }
}
