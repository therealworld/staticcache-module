<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use OxidEsales\Eshop\Core\Output as OxOutput;
use OxidEsales\Eshop\Core\ShopControl as OxShopControl;
use TheRealWorld\CacheCleanModule\Application\Controller\Admin\CacheCleanController as TrwCacheCleanController;
use TheRealWorld\StaticCacheModule\Application\Controller\Admin\CacheCleanController;
use TheRealWorld\StaticCacheModule\Core\Output;
use TheRealWorld\StaticCacheModule\Core\ShopControl;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwstaticcache',
    'title' => [
        'de' => 'the-real-world - statischer Cache',
        'en' => 'the-real-world - static Cache',
    ],
    'description' => [
        'de' => 'Bestimmte Controller erzeugen statische Caches und zeigen diese ggf. an.',
        'en' => 'Certain controllers create static caches and may display them.',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwstaticcache'),
    'author'    => 'Mario Lorenz, Idee: Oxid-Kochbuch',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'extend'    => [
        // Core
        OxOutput::class      => Output::class,
        OxShopControl::class => ShopControl::class,
        // Controller-Admin
        TrwCacheCleanController::class => CacheCleanController::class
    ],
    'settings' => [
        [
            'group' => 'trwcachetime',
            'name'  => 'iTRWCacheLifetime',
            'type'  => 'num',
            'value' => 3600,
        ],
        [
            'group' => 'trwcachecopyright',
            'name'  => 'bTRWCacheShowCopyright',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwcachecontent',
            'name'  => 'bTRWCachestartcontroller',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwcachecontent',
            'name'  => 'bTRWCachearticlelistcontroller',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwcachecontent',
            'name'  => 'bTRWCachearticledetailscontroller',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwcachecontent',
            'name'  => 'bTRWCachecontentcontroller',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwcachecontent',
            'name'  => 'bTRWCachelinkscontroller',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwcachecontent',
            'name'  => 'aTRWAdditionalCaches',
            'type'  => 'arr',
            'value' => ['newscontroller'],
        ],
        [
            'group' => 'trwcachebuild',
            'name'  => 'aTRWNotAllowedSessionVars',
            'type'  => 'arr',
            'value' => [],
        ],
        [
            'group' => 'trwcachebuild',
            'name'  => 'aTRWNotAllowedCookieVars',
            'type'  => 'arr',
            'value' => [],
        ],
        [
            'group' => 'trwcachebuild',
            'name'  => 'aTRWAllowedSessionVars',
            'type'  => 'arr',
            'value' => [],
        ],
        [
            'group' => 'trwcachebuild',
            'name'  => 'aTRWAllowedCookieVars',
            'type'  => 'arr',
            'value' => [],
        ],
        [
            'group' => 'trwcachebuild',
            'name'  => 'aTRWAllowedRequestVars',
            'type'  => 'arr',
            'value' => [],
        ],
    ],
];
