<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\StaticCacheModule\SchedulerTasks;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\SchedulerModule\Core\ISchedulerTask;
use TheRealWorld\StaticCacheModule\Core\StaticCacheConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsFile;
use TheRealWorld\ToolsPlugin\Core\ToolsLog;

/**
 * CleanUp Static Cache Task Class.
 */
class TaskCleanUpStaticCache implements ISchedulerTask
{
    /** String of a unix-style crontab, if its empty, then the task is only for manually run  */
    protected string $_sDefaultCrontab = '10 2 * * *'; // daily at 2:10 am

    /** Number of the next step */
    protected int $_iNextStep = 0;

    /** Number of the next sub step */
    protected int $_iNextSubStep = 0;

    /** Returns the default crontab of the Task */
    public function getDefaultCrontab(): string
    {
        return $this->_sDefaultCrontab;
    }

    /**
     * get Path for "manual" Files.
     *
     * @param string $sPathKey - a possible key for several paths
     */
    public function getPathForManualFiles(string $sPathKey = ''): string
    {
        return '';
    }

    /**
     * get List of "manual" Files.
     *
     * @param string $sPathKey - a possible key for several paths
     */
    public function getManualFileList(string $sPathKey = ''): array
    {
        return [];
    }

    /** Install the Task */
    public function install(): bool
    {
        return true;
    }

    /**
     * Run the Task.
     *
     * @param int  $iCurrentStep    - The number of the current step
     * @param int  $iCurrentSupStep - The number of the current sub step
     * @param bool $bRunManually    - run the task manually?
     */
    public function run(int $iCurrentStep = 0, int $iCurrentSupStep = 0, bool $bRunManually = false): bool
    {
        $this->_iNextStep = $iCurrentStep;
        $this->_iNextSubStep = $iCurrentSupStep;

        ToolsFile::deleteFilesFromPath(
            StaticCacheConfig::getCachePath(),
            StaticCacheConfig::getCacheLifetime()
        );

        ToolsLog::setLogEntry(
            Registry::getLang()->translateString('TRWSTATICCACHE_CLEANUP_SUCCESSFULLY'),
            __CLASS__ . ' - ' . __FUNCTION__
        );

        return true;
    }

    /**
     * Get the number of the next step.
     *
     * @param bool $bSub - The Sub-Step?
     */
    public function getNextStep(bool $bSub = false): int
    {
        return $bSub ? $this->_iNextSubStep : $this->_iNextStep;
    }
}
