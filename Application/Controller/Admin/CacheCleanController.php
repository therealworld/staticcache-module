<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\StaticCacheModule\Application\Controller\Admin;

use TheRealWorld\StaticCacheModule\Core\StaticCacheConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsFile;

/**
 * provides a Cache Cleaner
 * Admin Menu: Service -> Cache Cleaner.
 */
class CacheCleanController extends CacheCleanController_parent
{
    /** run Cache Clean */
    public function runCleanCache(): void
    {
        ToolsFile::deleteFilesFromPath(
            StaticCacheConfig::getCachePath()
        );

        parent::runCleanCache();
    }
}
