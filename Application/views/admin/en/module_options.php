<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwcachetime'       => 'Cache Time',
    'SHOP_MODULE_GROUP_trwcachecopyright'  => 'Cache mit Copyright',
    'SHOP_MODULE_GROUP_trwcachecontent'    => 'Which contents should be cached?',
    'SHOP_MODULE_GROUP_trwcachebuild'      => 'build Cache if ...',

    'SHOP_MODULE_iTRWCacheLifetime'                 => 'Cache Lifetime in Sekunden',
    'SHOP_MODULE_bTRWCachestartcontroller'          => 'Startpage',
    'SHOP_MODULE_bTRWCachearticlelistcontroller'    => 'Articlelists',
    'SHOP_MODULE_bTRWCachearticledetailscontroller' => 'Articledetailpages',
    'SHOP_MODULE_bTRWCachelinkscontroller'          => 'Linklists',
    'SHOP_MODULE_bTRWCachecontentcontroller'        => 'Contentpages',
    'SHOP_MODULE_aTRWAdditionalCaches'              => 'List of other frontend-controllers to be cached (a frontend-controller with namespace per line, for example: newscontroller)',
    'SHOP_MODULE_aTRWNotAllowedSessionVars'         => 'Do not show the static cache if these session variables exist',
    'SHOP_MODULE_aTRWNotAllowedCookieVars'          => 'Do not show the static cache if these cookies are set',
    'SHOP_MODULE_aTRWAllowedSessionVars'            => 'Take this session variable into account in the static cache',
    'SHOP_MODULE_aTRWAllowedCookieVars'             => 'Take this cookie variable into account in the static cache',
    'SHOP_MODULE_aTRWAllowedRequestVars'            => 'Take this request variable into account in the static cache',
    'SHOP_MODULE_bTRWCacheShowCopyright'            => 'show Oxid-Copyright in cached Sourcecode?',

];
