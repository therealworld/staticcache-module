<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

include_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR .
    'de' .
    DIRECTORY_SEPARATOR . basename(__FILE__);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array_merge(
    $aLang,
    [
        'TRWSTATICCACHE_CLEANUP_SUCCESSFULLY' => 'Bereinigung des statischen Caches erfolgt',
    ]
);
