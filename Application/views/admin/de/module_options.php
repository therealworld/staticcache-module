<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwcachetime'       => 'Cache Dauer',
    'SHOP_MODULE_GROUP_trwcachecopyright'  => 'Cache mit Copyright',
    'SHOP_MODULE_GROUP_trwcachecontent'    => 'Welche Inhalte sollen zwischen gespeichert werden?',
    'SHOP_MODULE_GROUP_trwcachebuild'      => 'Cache bauen wenn ...',

    'SHOP_MODULE_iTRWCacheLifetime'                 => 'Zwischenspeicher Lebenszeit in Sekunden',
    'SHOP_MODULE_bTRWCachestartcontroller'          => 'Startseite',
    'SHOP_MODULE_bTRWCachearticlelistcontroller'    => 'Artikellisten',
    'SHOP_MODULE_bTRWCachearticledetailscontroller' => 'Artikeldetailseiten',
    'SHOP_MODULE_bTRWCachelinkscontroller'          => 'Linklisten',
    'SHOP_MODULE_bTRWCachecontentcontroller'        => 'Inhaltsseiten',
    'SHOP_MODULE_aTRWAdditionalCaches'              => 'Liste weiterer Frontend-Controller, die zwischen gespeichert werden sollen (ein Frontend-Controller, klein geschrieben, ohne Namespace, je Zeile z.B: newscontroller)',
    'SHOP_MODULE_aTRWNotAllowedSessionVars'         => 'statischen Cache nicht zeigen, wenn diese Session-Variablen existieren',
    'SHOP_MODULE_aTRWNotAllowedCookieVars'          => 'statischen Cache nicht zeigen, wenn diese Cookies gesetzt sind',
    'SHOP_MODULE_aTRWAllowedSessionVars'            => 'Beim statischen Cache diese Session-Variable berücksichtigen',
    'SHOP_MODULE_aTRWAllowedCookieVars'             => 'Beim statischen Cache diese Cookie-Variable berücksichtigen',
    'SHOP_MODULE_aTRWAllowedRequestVars'            => 'Beim statischen Cache diese Request-Variable berücksichtigen',
    'SHOP_MODULE_bTRWCacheShowCopyright'            => 'Oxid-Copyright im gecachten Quellcode anzeigen?',
];
